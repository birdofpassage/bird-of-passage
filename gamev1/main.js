var mainState = {
    preload: function () {
        // g�rseli y�kle
        game.load.image('bird', 'assets/bird.png');
    },

    create: function () {
       
        game.stage.backgroundColor = '#71c5cf';

        game.physics.startSystem(Phaser.Physics.ARCADE);

        // ilk olarak ba�lanacak yer x=100 ve y=245
        this.bird = game.add.sprite(100, 245, 'bird');

        // hareketler ve yer�ekimi i�in fizik motoru
        game.physics.arcade.enable(this.bird);

        // ilk yer�ekimi
        this.bird.body.gravity.x = -1000;

        // space e bas�nca z�pla
        var spaceKey = game.input.keyboard.addKey(
                        Phaser.Keyboard.SPACEBAR);
        spaceKey.onDown.add(this.jump, this);
    },

    update: function () {
        // yer�ekimini ayarla
        if (this.bird.x < 401 && this.bird.x != 2) {
            this.bird.body.gravity.x = -1000;
        } else if (this.bird.x > 400 && this.bird.x != 750) {
            this.bird.body.gravity.x = 1000;
        }
        //ekran�n d���na ��kma
        if (this.bird.x <= 1) {
            this.bird.body.gravity.x = 0;
            this.bird.body.velocity.x = 0;
            this.bird.x = 2;
        } else if (this.bird.x >= 751) {
            this.bird.body.gravity.x = 0;
            this.bird.body.velocity.x = 0;
            this.bird.x = 750;
        }


        //this.restartGame();
        
    },
    // z�plama olay�
    jump: function () {
        // sa�dan veya soldan kuvvet uygula
        if (this.bird.x < 401)
            this.bird.body.velocity.x = +500;
        else
            this.bird.body.velocity.x = -500;


    },

    // Oyunu yeniden ba�lat
    restartGame: function () {
        game.state.start('main');
    },
};
 
var game = new Phaser.Game(800, 490);

game.state.add('main', mainState, true);