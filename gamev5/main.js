
var loop1 = 0;
var stop = 0;
var step = 3;
var steparea = 1;
var fishscore = 0;
var fishx = 0;

var mainState = {
    preload: function () {
        // g�rseli y�kle
        game.load.image('bird', 'assets/kus.png');
        game.load.image('birdr', 'assets/kusc.png',50,50);
        // engeli y�kle
        game.load.image('ice', 'assets/bulut.png');
        game.load.image('fish', 'assets/fish.png');
        game.load.image('background', 'assets/background2.jpg');
        game.load.image('island', 'assets/island.png');
        game.load.spritesheet('button', 'assets/button_sprite_sheet.png', 193, 71);
    },

    create: function () {
        //game.stage.backgroundColor = '#71c5cf';

        this.timeDelay = 0;

        this.bg = game.add.sprite(0, 0, 'background');
        this.island = game.add.sprite(0, -450, 'island');
        game.physics.startSystem(Phaser.Physics.ARCADE);

        // ilk olarak ba�lanacak yer x=100 ve y=245

        this.bird = game.add.sprite(100, 345, 'bird');
        
        // hareketler ve yer�ekimi i�in fizik motoru
        game.physics.arcade.enable(this.bird);

        // ilk yer�ekimi
        this.bird.body.gravity.x = -1000;

        // space e bas�nca z�pla
        var spaceKey = game.input.keyboard.addKey(
                        Phaser.Keyboard.SPACEBAR);
        spaceKey.onDown.add(this.jump, this);



        //engel k�sm�
        this.ices = game.add.group();
        this.fish = game.add.group();
        this.timer = game.time.events.loop(2250, this.addRowOfices, this);

        //skor k�sm�
        this.score = -1;
        this.labelScore = game.add.text(20, 20, "0",
            { font: "30px Arial", fill: "#ffffff" });
        this.labelScore4 = game.add.text(20, 60, "Distence : "  + -(this.island.y + 180) + " km",
            { font: "15px Arial", fill: "#ffffff" });

        this.labelScore3 = game.add.text(290, 180, "Game Over",
            { font: "40px Arial", fill: "#ffffff" });

        this.labelScore3_ = game.add.text(320, 180, "You Win!",
            { font: "40px Arial", fill: "#ffffff" });

        this.labelScore2 = game.add.text(320, 250, "0",
            { font: "25px Arial", fill: "#ffffff" });

        this.button1 = game.add.button(300, 350, 'button', actionOnClick, this, 2, 1, 0);

        this.labelScore2.visible = false;
        this.labelScore3.visible = false;
        this.labelScore3_.visible = false;
        this.button1.visible = false;
        // this.button = game.add.button(game.world.centerX, 270, 'button', actionOnClick, this, 2, 1, 0);

    },
    //deneme

    update: function () {
        // yer�ekimini ayarla
        if (this.bird.x < 401 && this.bird.x != 2) {
            this.bird.body.gravity.x = -1000;
        } else if (this.bird.x > 400 && this.bird.x != 750) {
            this.bird.body.gravity.x = 1000;
          

        }
        //ekran�n d���na ��kma
        if (this.bird.x <= 1) {
            this.bird.body.gravity.x = 0;
            this.bird.body.velocity.x = 0;
            this.bird.x = 2;
        } else if (this.bird.x >= 751) {
            this.bird.body.gravity.x = 0;
            this.bird.body.velocity.x = 0;
            this.bird.x = 750;
        }

        game.physics.arcade.overlap(
       this.bird, this.ices, this.stopGame, null, this);
        if (this.bird.visible) game.physics.arcade.overlap(
        this.bird, this.fish, this.boostGame, null, this);

        if (this.island.y <= -520) {
            stop = 1;
            this.labelScore.visible = false;
            this.labelScore2.visible = true;
            this.labelScore3.visible = true;
            this.button1.visible = true;
        }
        if (this.island.y >= -180) {

            stop = 1;
            this.labelScore.visible = false;
            this.labelScore2.visible = true;
            this.labelScore3_.visible = true;
            this.button1.visible = true;
        }
        //yeniden ba�lat (skordan sonra)
        //this.restartGame();
    },
    // z�plama olay�
    jump: function () {
        // sa�dan veya soldan kuvvet uygula
        if (this.bird.x < 401)
            this.bird.body.velocity.x = +400;
        else
            this.bird.body.velocity.x = -400;


    },

    // Oyunu yeniden ba�lat
    restartGame: function () {
        game.state.start('main');
    },// Oyunu yeniden ba�lat
    stopGame: function () {
        //stop = 1;
        
        this.island.y = this.island.y - 1;

        this.labelScore4.text = "Distence : " + -(this.island.y + 180) + " km";
        //this.bird.body.velocity.y = +250;

        //this.labelScore.visible = false;
        //this.labelScore2.visible = true;
        //this.labelScore3.visible = true;
        //this.button1.visible = true;
    },
    boostGame: function () {
        //this.fish.kill;

        //if (game.time.now > this.timeDelay) {
        //this.fish.visible = false;

        fishscore = fishscore + 1;
        //if (fishscore == 2) {

        this.island.y = this.island.y + 10;
        this.fish.y = this.fish.y + 300;
        fishx = 1;

        //this.restartGame();

        // wait 1 second
        //     timeDelay = game.time.now + 1000
        // }
        // }
        //    return false;

    },
    //engel k�sm�
    addOneice: function (x, y) {
        // Create a ice at the position x and y
        var ice = game.add.sprite(x, y, 'ice');

        // Add the ice to our previously created group
        this.ices.add(ice);

        // engel fizik motoru
        game.physics.arcade.enable(ice);

        // engelin yer�ekimi
        ice.body.velocity.y = +150;

        // engel ekrandan ��k�nca sil
        ice.checkWorldBounds = true;
        ice.outOfBoundsKill = true;
    },
    addOnefish: function (x, y) {
        // Create a ice at the position x and y
        var fish = game.add.sprite(x, y, 'fish');

        // Add the ice to our previously created group
        this.fish.add(fish);

        // engel fizik motoru
        game.physics.arcade.enable(fish);

        // engelin yer�ekimi
        fish.body.velocity.y = +150;

        // engel ekrandan ��k�nca sil
        fish.checkWorldBounds = true;
        fish.outOfBoundsKill = true;
    },
    addRowOfices: function () {

        if (stop == 0) {
            loop1 = loop1 + 1;
            loop1 = loop1 % 6;

            // random say� al 
            var hole = Math.floor(Math.random() * (3) + 1);
            var hole2 = Math.floor(Math.random() * (3) + 7);

            if (loop1 == step) {
                if (fishx == 1) {
                    fishx = 0;
                    this.fish.y = this.fish.y - 300;
                } this.fish.visible = true;

                step = Math.floor(Math.random() * (3) + 2);
                steparea = Math.floor(Math.random() * (2) + 1);

                if (steparea == 1)
                    this.addOnefish((hole + 1) * 60 + 10, 0);
                else
                    this.addOnefish((hole2 + 1) * 60 + 10, 0);

            }


            this.addOneice(10, 0);
            this.addOneice(12 * 60 + 10, 0);
            // 4 tane engel koy
            for (var i = 0; i < 13; i++)
                if (i != hole && i != hole + 1 && i != hole + 2 && i != hole2 && i != hole2 + 1 && i != hole2 + 2)
                    this.addOneice(i * 60 + 10, 0);


            //skor k�sm�

            this.score += 1;
            this.island.y = this.island.y + 1;
            if (this.score > 0)
                beep();

            this.labelScore.text = this.score;
            this.labelScore2.text = "Your score : " + this.score;
            this.labelScore4.text = "Distence : " + -(this.island.y + 180) + " km";

        }
    },
};
function beep() {
    var snd = new Audio("C:\\Users\\serhat\\Desktop\\game\\GamePractical1\\GamePractical1\\bin\\sound\\next.wav");
    snd.play();
}
var game = new Phaser.Game(800, 490);

game.state.add('main', mainState, true);



function actionOnClick() {
    stop = 0;
    this.restartGame();
}