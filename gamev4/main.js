
var loop1 = 0;
var stop = 0;
var step = 5;
var steparea = 1;
var mainState = {
    preload: function () {
        // g�rseli y�kle
        game.load.image('bird', 'assets/kus.png');
        // engeli y�kle
        game.load.image('cloud', 'assets/ice.png');
        game.load.image('fish', 'assets/fish.png');
        game.load.image('background', 'assets/background.jpg');
        game.load.spritesheet('button', 'assets/button_sprite_sheet.png', 193, 71);
    },

    create: function () {
        //game.stage.backgroundColor = '#71c5cf';
        this.bg = game.add.sprite(0, 0, 'background');
        game.physics.startSystem(Phaser.Physics.ARCADE);

        // ilk olarak ba�lanacak yer x=100 ve y=245
        this.bird = game.add.sprite(100, 345, 'bird');

        // hareketler ve yer�ekimi i�in fizik motoru
        game.physics.arcade.enable(this.bird);

        // ilk yer�ekimi
        this.bird.body.gravity.x = -1000;

        // space e bas�nca z�pla
        var spaceKey = game.input.keyboard.addKey(
                        Phaser.Keyboard.SPACEBAR);
        spaceKey.onDown.add(this.jump, this);



        //engel k�sm�
        this.clouds = game.add.group();
        this.timer = game.time.events.loop(1250, this.addRowOfclouds, this);

        //skor k�sm�
        this.score = -1;
        this.labelScore = game.add.text(20, 20, "0",
            { font: "30px Arial", fill: "#ffffff" });

        this.labelScore3 = game.add.text(290, 180, "Game Over",
            { font: "40px Arial", fill: "#ffffff" });
        this.labelScore2 = game.add.text(320, 250, "0",
            { font: "25px Arial", fill: "#ffffff" });

        this.button1 = game.add.button(350, 350, 'button', actionOnClick, this, 2, 1, 0);

        this.labelScore2.visible = false;
        this.labelScore3.visible = false;
        this.button1.visible = false;
        // this.button = game.add.button(game.world.centerX, 270, 'button', actionOnClick, this, 2, 1, 0);

    },

    update: function () {
        // yer�ekimini ayarla
        if (this.bird.x < 401 && this.bird.x != 2) {
            this.bird.body.gravity.x = -1000;
        } else if (this.bird.x > 400 && this.bird.x != 750) {
            this.bird.body.gravity.x = 1000;
        }
        //ekran�n d���na ��kma
        if (this.bird.x <= 1) {
            this.bird.body.gravity.x = 0;
            this.bird.body.velocity.x = 0;
            this.bird.x = 2;
        } else if (this.bird.x >= 751) {
            this.bird.body.gravity.x = 0;
            this.bird.body.velocity.x = 0;
            this.bird.x = 750;
        }

        game.physics.arcade.overlap(
        this.bird, this.clouds, this.stopGame, null, this);
        //yeniden ba�lat (skordan sonra)
        //this.restartGame();
    },
    // z�plama olay�
    jump: function () {
        // sa�dan veya soldan kuvvet uygula
        if (this.bird.x < 401)
            this.bird.body.velocity.x = +400;
        else
            this.bird.body.velocity.x = -400;


    },

    // Oyunu yeniden ba�lat
    restartGame: function () {
        game.state.start('main');
    },// Oyunu yeniden ba�lat
    stopGame: function () {
        stop = 1;

        this.bird.body.velocity.y = +250;

        this.labelScore.visible = false;
        this.labelScore2.visible = true;
        this.labelScore3.visible = true;
        this.button1.visible = true;
    },

    //engel k�sm�
    addOnecloud: function (x, y) {
        // Create a cloud at the position x and y
        var cloud = game.add.sprite(x, y, 'cloud');

        // Add the cloud to our previously created group
        this.clouds.add(cloud);

        // engel fizik motoru
        game.physics.arcade.enable(cloud);

        // engelin yer�ekimi
        cloud.body.velocity.y = +250;

        // engel ekrandan ��k�nca sil
        cloud.checkWorldBounds = true;
        cloud.outOfBoundsKill = true;
    },
    addOnefish: function (x, y) {
        // Create a cloud at the position x and y
        var fish = game.add.sprite(x, y, 'fish');

        // Add the cloud to our previously created group
        this.clouds.add(fish);

        // engel fizik motoru
        game.physics.arcade.enable(fish);

        // engelin yer�ekimi
        fish.body.velocity.y = +250;

        // engel ekrandan ��k�nca sil
        fish.checkWorldBounds = true;
        fish.outOfBoundsKill = true;
    },
    addRowOfclouds: function () {
        
        if (stop == 0) {
            loop1 = loop1 + 1;
            loop1 = loop1 % 16;

            // random say� al 
            var hole = Math.floor(Math.random() * (3) + 1);
            var hole2 = Math.floor(Math.random() * (3) + 7);

            if (loop1 == step) {
                step = Math.floor(Math.random() * (10) + 5);
                steparea = Math.floor(Math.random() * (2));

                if (steparea == 1)
                    this.addOnefish((hole + 1) * 60 + 10, 0);
                else
                    this.addOnefish((hole2 + 1) * 60 + 10, 0);

            }
           

            this.addOnecloud(10, 0);
            this.addOnecloud(12 * 60 + 10, 0);
            // 4 tane engel koy
            for (var i = 0; i < 13; i++)
                if (i != hole && i != hole + 1 && i != hole + 2  && i != hole2 && i != hole2 + 1 && i != hole2 + 2)
                    this.addOnecloud(i * 60 + 10, 0);


            //skor k�sm�

            this.score += 1;
            if (this.score > 0)
                beep();

            this.labelScore.text = this.score;
            this.labelScore2.text = "Your score : " + this.score;

        }
    },
};
function beep() {
    var snd = new Audio("C:\\Users\\serhat\\Desktop\\game\\GamePractical1\\GamePractical1\\bin\\sound\\next.wav");
    snd.play();
}
var game = new Phaser.Game(800, 490);

game.state.add('main', mainState, true);



function actionOnClick() {
    stop = 0;
    this.restartGame();
}