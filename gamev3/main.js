var mainState = {
    preload: function () {
        // g�rseli y�kle
        game.load.image('bird', 'assets/bird.png');
        // engeli y�kle
        game.load.image('pipe', 'assets/pipe.png');
    },

    create: function () {
       
        game.stage.backgroundColor = '#71c5cf';

        game.physics.startSystem(Phaser.Physics.ARCADE);

        // ilk olarak ba�lanacak yer x=100 ve y=245
        this.bird = game.add.sprite(100, 345, 'bird');

        // hareketler ve yer�ekimi i�in fizik motoru
        game.physics.arcade.enable(this.bird);

        // ilk yer�ekimi
        this.bird.body.gravity.x = -1000;

        // space e bas�nca z�pla
        var spaceKey = game.input.keyboard.addKey(
                        Phaser.Keyboard.SPACEBAR);
        spaceKey.onDown.add(this.jump, this);

        //engel k�sm�

        this.pipes = game.add.group();

        this.timer = game.time.events.loop(2000, this.addRowOfPipes, this);

        //skor k�sm�
        this.score = 0;
        this.labelScore = game.add.text(20, 20, "0",
            { font: "30px Arial", fill: "#ffffff" });
    },

    update: function () {
        // yer�ekimini ayarla
        if (this.bird.x < 401 && this.bird.x != 2) {
            this.bird.body.gravity.x = -1000;
        } else if (this.bird.x > 400 && this.bird.x != 750) {
            this.bird.body.gravity.x = 1000;
        }
        //ekran�n d���na ��kma
        if (this.bird.x <= 1) {
            this.bird.body.gravity.x = 0;
            this.bird.body.velocity.x = 0;
            this.bird.x = 2;
        } else if (this.bird.x >= 751) {
            this.bird.body.gravity.x = 0;
            this.bird.body.velocity.x = 0;
            this.bird.x = 750;
        }
        //yeniden ba�lat (skordan sonra)
        game.physics.arcade.overlap(
        this.bird, this.pipes, this.restartGame, null, this);
        //this.restartGame();
    },
    // z�plama olay�
    jump: function () {
        // sa�dan veya soldan kuvvet uygula
        if (this.bird.x < 401)
            this.bird.body.velocity.x = +400;
        else
            this.bird.body.velocity.x = -400;


    },

    // Oyunu yeniden ba�lat
    restartGame: function () {
        game.state.start('main');
    },
    
    //engel k�sm�
    addOnePipe: function (x, y) {
        // Create a pipe at the position x and y
        var pipe = game.add.sprite(x, y, 'pipe');

        // Add the pipe to our previously created group
        this.pipes.add(pipe);

        // engel fizik motoru
        game.physics.arcade.enable(pipe);

        // engelin yer�ekimi
        pipe.body.velocity.y = +200;

        // engel ekrandan ��k�nca sil
        pipe.checkWorldBounds = true;
        pipe.outOfBoundsKill = true;
    },
    addRowOfPipes: function () {
        // random say� al 
        var hole = Math.floor(Math.random() * 10) ;

        // 4 tane engel koy
        for (var i = 0; i < 13; i++)
            if (i == hole || i == hole + 1 || i == hole + 2 || i == hole + 3)
                this.addOnePipe(i * 60 + 10, 0);

        //skor k�sm�
        this.score += 1;
        this.labelScore.text = this.score;
    },
};
 
var game = new Phaser.Game(800, 490);

game.state.add('main', mainState, true);



